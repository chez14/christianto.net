import {
  Fredoka,
  Kosugi_Maru,
  Noto_Sans,
  Noto_Sans_JP,
  Noto_Emoji,
} from "next/font/google";

export const fredoka = Fredoka({
  weight: ["300", "400", "500", "600"],
  display: "swap",
  adjustFontFallback: false,
  variable: "--font-fredoka",
  subsets: ["latin", "latin-ext"],
});
export const kosugiMaru = Kosugi_Maru({
  display: "swap",
  adjustFontFallback: false,
  weight: "400",
  variable: "--font-kosugiMaru",
  subsets: ["latin", "latin-ext"],
});
export const notoSans = Noto_Sans({
  display: "swap",
  adjustFontFallback: false,
  weight: ["100", "200", "300", "400", "500", "600", "700"],
  variable: "--font-notoSans",
  subsets: ["latin", "latin-ext"],
});
export const notoSansJp = Noto_Sans_JP({
  weight: "variable",
  display: "swap",
  adjustFontFallback: false,
  variable: "--font-notoSansJp",
  subsets: ["latin", "latin-ext"],
});
export const notoEmoji = Noto_Emoji({
  weight: "variable",
  display: "swap",
  adjustFontFallback: false,
  variable: "--font-notoEmoji",
  subsets: ["emoji"],
});
