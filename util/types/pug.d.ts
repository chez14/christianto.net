/**
 * Pug files. Will be loaded as file loaders, when you import this file, this
 * variable will be filled with path to the file.
 */
declare module '*.pug' {
  const path: any;
  export default path;
}
