import { BlogPost } from "~/data/blogs/blog-post";
import { WpApi } from "./api";

export async function getBlogPosts(extraParams: any = undefined): Promise<BlogPost[]> {
  const postsResponse = await WpApi.get('wp/v2/posts', {
    params: {
      '_embed': ['author', 'wp:featuredmedia', 'wp:term'],
      status: 'publish',
      ...extraParams
    }
  });

  const posts = postsResponse.data.map((post: any) => {
    const reconstructedPostData = {
      id: post.id,
      title: post.title.rendered,
      date: post.date,
      link: post.link,
      excerpt: post.excerpt.rendered,
      author: post._embedded.author.map((author: any) => ({
        name: author.name,
        link: author.link,
      })),
      featuredImage: {},
      categories: post._embedded['wp:term']?.[0]?.map((category: any) => ({
        name: category.name,
        link: category.link,
      })),
      tags: post._embedded['wp:term']?.[1]?.map((tag: any) => ({
        name: tag.name,
        link: tag.link,
      }))
    }

    if (post._embedded && post._embedded['wp:featuredmedia']) {
      reconstructedPostData.featuredImage = post._embedded['wp:featuredmedia'][0]['media_details']['sizes']['full'];
      if (post._embedded['wp:featuredmedia'][0]['media_details']['sizes']?.['medium_large']) {
        reconstructedPostData.featuredImage = post._embedded['wp:featuredmedia'][0]['media_details']['sizes']['medium_large'];
      }
    }

    return reconstructedPostData;
  });

  return posts;
}
