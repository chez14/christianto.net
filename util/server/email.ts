import nodemailer from "nodemailer";

export const smtpTransport = nodemailer.createTransport({
  host: process.env.NEXT_SMTP_HOST || "smtp.mailgun.org",
  port: Number.parseInt(process.env.NEXT_SMTP_PORT || "465"),
  secure: (process.env.NEXT_SMTP_SECURE === "true"),
  auth: {
    user: process.env.NEXT_SMTP_USERNAME || "",
    pass: process.env.NEXT_SMTP_PASSWORD || "",
  }
})

