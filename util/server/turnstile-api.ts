import Axios from "axios";

const turnstileCaller = Axios.create({
  baseURL: 'https://challenges.cloudflare.com/turnstile/v0/',
  headers: { "Accept-Encoding": "gzip,deflate,compress" }
});

export async function validateTurnstileCaptcha(response: string, remoteIp?: string): Promise<boolean> {
  let resp = await turnstileCaller.post('siteverify', {
    secret: process.env.NEXT_TURNSTILE_SECRET,
    response: response,
    remoteip: remoteIp,
  });

  if (resp.data.success) {
    return true;
  }

  console.error("Captcha error:", resp.data['error-codes']);
  return false;
}
