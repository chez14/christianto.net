import axios from "axios";

export const WpApi = axios.create({
  baseURL: process.env.WP_ENDPOINT,
  responseType: "json",
  headers: { "Accept-Encoding": "gzip,deflate,compress" }
});

export const frontendApiCaller = axios.create({
  baseURL: "/api",
  responseType: "json",
  headers: {
    "Accept": "application/json"
  }
});


export const portmafiaApi = axios.create({
  baseURL: `${process.env.NEXT_PUBLIC_PORTMAFIA_ENDPOINT}api/🌐/`,
  responseType: "json",
  headers: {
    "Accept": "application/json"
  }
});
