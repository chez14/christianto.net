export function getUrlBaseName(): URL {
  // fallbacks:
  let protocol = "https:";
  let hostname = process.env.NEXT_PUBLIC_URL;
  let port = 443;

  if (typeof window !== 'undefined') {
    protocol = window?.location?.protocol || protocol;
    hostname = window?.location?.hostname || hostname;
    port = Number.parseInt(window?.location?.port) || port;
  }

  let url = new URL("", `${protocol}//${hostname}:${port}`);

  return url;
}
