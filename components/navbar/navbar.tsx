"use client";

import classNames from 'classnames';
import Link from 'next/link';
import { Container, Nav, Navbar } from 'react-bootstrap';
import { When } from 'react-if';
import ExternalLinkIcon from '../utils/external-link-icon';
import Logo from './logo';
import styles from "./navbar.module.scss";

export interface NavbarComponentProps extends React.HTMLAttributes<HTMLDivElement> {
  bg?: string,
  variant?: string,
  logoVariant?: "dark" | "light" | "mono",
  addPadders?: boolean,
  routes?: string
}

export const defaultProps: Partial<NavbarComponentProps> = {
  addPadders: true,
  logoVariant: "dark",
  variant: "dark",
  routes: ""
}

function NavbarComponent(props: NavbarComponentProps) {
  const { bg, variant: variant, addPadders, logoVariant, className, routes } = { ...defaultProps, ...props } as Required<NavbarComponentProps>;

  return (
    <>
      <Navbar bg={bg} variant={variant} expand="lg" className={classNames([styles.navbar, className])} fixed='top' collapseOnSelect>
        <Container>
          <Link href="/" passHref legacyBehavior>
            <Navbar.Brand as={Nav.Link} aria-label='Home'>
              <Logo className={styles.logo} variant={logoVariant} />
            </Navbar.Brand>
          </Link>
          <Navbar.Toggle aria-controls="main-navbar" />
          <Navbar.Collapse id="main-navbar" className={styles.navbarMenu}>
            <Nav className="ms-auto" activeKey={routes}>
              <Link href="https://blog.christianto.net/?utm_source=home&utm_medium=navbar" passHref legacyBehavior>
                <Nav.Link>Blog <ExternalLinkIcon /></Nav.Link>
              </Link>
              <Link href="/works" passHref legacyBehavior>
                <Nav.Link key={"/works"}>Works</Nav.Link>
              </Link>
              <Link href="/about" passHref legacyBehavior>
                <Nav.Link key={"/about"}>About</Nav.Link>
              </Link>
              <Link href="/contact" passHref legacyBehavior>
                <Nav.Link key={"/contact"}>Contact</Nav.Link>
              </Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <When condition={addPadders}>
        <div className={classNames([styles.padders, className])} />
      </When>
    </>
  )
}

export default NavbarComponent
