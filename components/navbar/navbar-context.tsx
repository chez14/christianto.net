"use client";

import { Dispatch, SetStateAction, createContext, useContext, useEffect } from "react";
import NavbarComponent, { NavbarComponentProps, defaultProps } from "./navbar";

export type NavbarComponentProxy<S = NavbarComponentProps> = [S, Dispatch<SetStateAction<S>>];

export const NavbarComponentContext = createContext<NavbarComponentProxy | undefined>(undefined);


export function NavbarContext(props: NavbarComponentProps) {
  const [_, setProps] = useContext(NavbarComponentContext) || [];

  useEffect(() => {
    if (setProps) {
      setProps(props);
    }
  }, [props, setProps]);

  return <></>
}

export function ProxifiedNavbar() {
  const [props, _] = useContext(NavbarComponentContext) || [defaultProps, undefined];

  return (
    <NavbarComponent {...props} />
  )
}
