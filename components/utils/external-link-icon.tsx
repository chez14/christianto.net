import { faArrowUpRightFromSquare } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'

function ExternalLinkIcon() {
  return (
    <FontAwesomeIcon icon={faArrowUpRightFromSquare} style={{ fontSize: "70%" }} />
  )
}

export default ExternalLinkIcon
