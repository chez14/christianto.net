import React, { ReactNode } from 'react';

import styles from "./section-head.module.scss";

export interface SectionHeadProps extends React.HTMLProps<HTMLDivElement> {
  title: string,
  titleClass?: string,
  children: ReactNode,
  as?: keyof JSX.IntrinsicElements
}

function SectionHead({ title, titleClass, children, as = "h2" }: SectionHeadProps) {

  let ProxyClass = as;

  return (
    <div className={styles.sectionHead}>
      <div className={"section-header-title"}>
        <ProxyClass className={titleClass}>{title}</ProxyClass>
      </div>
      <div className="section-header-handle">
        {children}
      </div>
    </div>
  )
}

export default SectionHead
