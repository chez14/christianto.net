"use client";

import { faCopy } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import classNames from 'classnames'
import React, { useEffect, useState } from 'react'
import { Button, FormControl, InputGroup, OverlayTrigger, Tooltip } from 'react-bootstrap'
import { When } from 'react-if'

import styles from './copyable-text.module.scss';

export interface CopyableTextProps extends React.HTMLAttributes<HTMLDivElement> {
  content: string
}

let tooltipDecay: any = null;

function CopyableText({ content, className, ...props }: CopyableTextProps) {
  const [hasClipboardApi, setHasClipboardApi] = useState(false)
  const [copySuccessfull, setCopySuccessfull] = useState<undefined | boolean>(undefined)


  const tooltips = (opts: any) => (<Tooltip {...opts}>
    {copySuccessfull === undefined ? "Copy code" : (copySuccessfull ? "Copied!" : "Copy failed")}
  </Tooltip>);

  useEffect(() => {
    setHasClipboardApi(!!navigator?.clipboard);
    return () => { }
  }, []);

  async function copyContentToClipboard(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    e.preventDefault();
    try {
      await navigator.clipboard.writeText(content)
      setCopySuccessfull(true);
    } catch (_) {
      setCopySuccessfull(false);
    }
    if (tooltipDecay) {
      clearTimeout(tooltipDecay);
    }
    tooltipDecay = setTimeout(() => setCopySuccessfull(undefined), 2000);
  }

  return (
    <InputGroup className={classNames([styles.copyBody, className])} {...props} hasValidation>
      <FormControl readOnly value={content} className="font-monospace border-0" />
      <When condition={hasClipboardApi}>
        <OverlayTrigger
          delay={{ show: 250, hide: 400 }}
          overlay={tooltips}>
          <Button as='span' variant='secondary' onClick={(e) => copyContentToClipboard(e)}><FontAwesomeIcon icon={faCopy} /></Button>
        </OverlayTrigger>
      </When>
    </InputGroup>
  )
}

export default CopyableText
