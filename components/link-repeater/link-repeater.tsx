import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import { Button, Col, Row } from 'react-bootstrap'
import { When } from 'react-if'
import { LinkItem } from '~/data/contact/linksies'

import styles from "./link-repeater.module.scss"

export interface LinkRepeaterProps extends React.HTMLAttributes<HTMLDivElement> {
  links: LinkItem[]
}

function LinkRepeater({ links }: LinkRepeaterProps) {
  return (
    <Row className="align-items-stretch">
      {links.map(link => <Col key={link.link} xs={12} md={3} className="my-2">
        <Button href={link.link} className={styles.linkButton} variant="secondary" rel="noopener noreferrer" target="_blank">
          <span className="linkbutton-title">
            <When condition={!!link.icon}>
              <FontAwesomeIcon icon={link.icon} />
            </When>
            {link.title}
          </span>
          <span className="linkbutton-link">
            {link.link}
          </span>
        </Button>
      </Col>)}
    </Row>
  )
}

export default LinkRepeater
