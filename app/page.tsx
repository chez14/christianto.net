import { Metadata } from 'next';
import { Container } from 'react-bootstrap';
import { When } from 'react-if';
import profilePict from "~/assets/home/profile.jpg";
import { NavbarContext } from '~/components/navbar/navbar-context';
import { BlogPost } from '~/data/blogs/blog-post';
import { getBlogPosts } from '~/util/blogpost-fetcher';
import { getUrlBaseName } from '~/util/url-helper';
import HomeMainBanner from './_fragments/main-banner';
import HomeSectionBlogPost from './_fragments/section-blog-post';
import HomeSectionLatestProject from './_fragments/section-latest-project';

let urlBase = getUrlBaseName();

export const metadata: Metadata = {
  title: 'Home | Christianto', // idk why i can't just reuse the template ._.
  description: 'Hi there! This is Chris, also known as chez14 around the internet.',
  openGraph: {
    images: [
      {
        url: `${urlBase.origin}${profilePict.src}`,
        width: profilePict.width,
        height: profilePict.height,
        alt: "An image of Chris in anime style"
      }
    ]
  }
}

export const revalidate = 3600;

async function getPosts() {
  let blogPosts: BlogPost[] = [];

  let categoryFilters: string[] = (process.env.WP_PRIORITIZED_POST_CATEGORIES || "").split(",")

  try {
    blogPosts = (await getBlogPosts({ categories: categoryFilters }) ?? []).slice(0, 7);
  } catch (e) {
    console.error(e);
    return undefined;
  }
  return blogPosts;
}

async function Homepage() {
  const posts = await getPosts()
  return (
    <>
      <NavbarContext addPadders={false} bg='dark' logoVariant='dark' />

      <section className="mb-5">
        <HomeMainBanner />
      </section>
      <Container>
        <div className="my-5">
          <HomeSectionLatestProject />
        </div>

        <When condition={!!posts}>
          <div className="my-5">
            <HomeSectionBlogPost blogs={posts || []} />
          </div>
        </When>
      </Container>
    </>
  )
}

export default Homepage
