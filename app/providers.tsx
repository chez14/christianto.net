'use client'

import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { HTMLAttributes, useState } from 'react'
import { defaultProps } from '~/components/navbar/navbar'
import { NavbarComponentContext } from '~/components/navbar/navbar-context'

function makeQueryClient() {
  return new QueryClient({
    defaultOptions: {
      queries: {
        staleTime: 60 * 1000,
      },
    },
  })
}

let browserQueryClient: QueryClient | undefined = undefined

function getQueryClient() {
  if (typeof window === 'undefined') {
    // Server: always make a new query client
    return makeQueryClient()
  } else {
    if (!browserQueryClient) {
      browserQueryClient = makeQueryClient()
    }
    return browserQueryClient
  }
}

export default function Providers({ children }: HTMLAttributes<HTMLElement>) {
  const queryClient = getQueryClient()
  const navbarContextObject = useState(defaultProps)

  return (
    <QueryClientProvider client={queryClient}>
      <NavbarComponentContext.Provider value={navbarContextObject}>
        {children}
      </NavbarComponentContext.Provider>
    </QueryClientProvider>
  )
}
