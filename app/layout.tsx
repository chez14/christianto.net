import { GoogleAnalytics } from '@next/third-parties/google'
import { Metadata, Viewport } from "next"
import '../styles/globals.scss'
import Providers from "./providers"

import { config } from '@fortawesome/fontawesome-svg-core'
import FooterComponent from '~/components/footer/footer'
import { ProxifiedNavbar } from '~/components/navbar/navbar-context'
config.autoAddCss = false

import {
  fredoka,
  kosugiMaru,
  notoEmoji,
  notoSans,
  notoSansJp,
} from "../util/fonts"
import classNames from 'classnames'


export const metadata: Metadata = {
  title: {
    default: 'Christianto',
    template: '%s | Christianto'
  },
  twitter: {
    card: "summary_large_image",
    site: "heychez14",
  },
  openGraph: {
    type: "website",
    locale: "en",
    siteName: "Christianto",
  }
}

export const viewport: Viewport = {
  themeColor: "#ee2737"
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html
      lang="en"
      data-bs-theme="dark"
      className={classNames(
        fredoka.variable,
        kosugiMaru.variable,
        notoSans.variable,
        notoSansJp.variable,
        notoEmoji.variable,
      )}>

      <head>
        <link rel="icon" href="/favicon.ico" />
        <link rel="icon" href="/icon.svg" type="image/svg+xml" />
        <link rel="apple-touch-icon" href="/apple-touch-icon.png" />
        <link rel="manifest" href="/manifest.webmanifest" />
      </head>

      <body>
        <GoogleAnalytics gaId={process.env.NEXT_PUBLIC_GA_MEASUREMENT_ID || ""} />
        <Providers>
          <ProxifiedNavbar />
          {children}
          <FooterComponent />
        </Providers>
      </body>

    </html>
  )
}
