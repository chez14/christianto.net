import { Metadata } from "next";
import { Col, Container, Row } from "react-bootstrap";
import CopyableText from "~/components/copyable-text/copyable-text";
import { NavbarContext } from "~/components/navbar/navbar-context";


export const metadata: Metadata = {
  title: "PGP Key",
  description: "Get Chris' PGP Public Key. Write secret messages, validate packages, and verify his signs.",
}


function PgpPage() {
  return (
    <>
      <NavbarContext addPadders bg='dark' logoVariant='dark' />

      <div className="my-3 mb-5">
        <Container>
          <h1>PGP Key</h1>
        </Container>
      </div>

      <Container>
        <Row>
          <Col xs="12" md="8">
            <h2>Obtaining the Key</h2>
            <p>
              You can always contact me with PGP-encrypted messages, verify my signature, or validate the packages or commit I made.
              Here{`’`}s some way to get those public keys:
            </p>

            <div className="my-5">
              <h3>Grab it via the <code>gpg</code> cli</h3>
              <p>
                This is the most recommended way of getting my public key for most cases. You can just run these in your terminal,
                as long as you have GPG program in your machine.
              </p>

              <ul>
                <li className="my-3">
                  <p className="mb-1">via MIT's Key Server</p>
                  <CopyableText content="gpg --keyserver pgp.mit.edu --recv-keys 332d7eafbcdb79a3" />
                </li>
                <li className="my-3">
                  <p className="mb-1">via Ubuntu's Key Server</p>
                  <CopyableText content="gpg --keyserver keyserver.ubuntu.com --recv-keys 332d7eafbcdb79a3" />
                </li>
              </ul>
            </div>

            <div className="my-5">
              <h3>Manually Add It to Your Keyring</h3>
              <p>
                For several cases, it is much easier to add the public key file manually rather than importing them via
                GPG program directly. For those cases, please refer to the following server to obtain my public key:
              </p>
              <ul>
                <li className="my-3">
                  <p className="mb-1">pgp.mit.edu</p>
                  <CopyableText content="https://pgp.mit.edu/pks/lookup?op=vindex&search=0x332D7EAFBCDB79A3" />
                </li>
                <li className="my-3">
                  <p className="mb-1">keyserver.ubuntu.com</p>
                  <CopyableText content="https://keyserver.ubuntu.com/pks/lookup?op=vindex&search=0x332d7eafbcdb79a3" />
                </li>
                <li className="my-3">
                  <p className="mb-1">static hosting</p>
                  <CopyableText content="https://gl-statis.konten.christianto.net/pgp/chris.pgp.asc" />
                </li>
              </ul>

              <p>Then you can add the file via Terminal:</p>
              <pre className="bg-black p-3">
                {`## Static file
cat chris.pgp.asc | gpg --import

## With cUrl
curl https://gl-statis.konten.christianto.net/pgp/chris.pgp.asc | gpg --import`}
              </pre>
            </div>

            <div className="my-5">
              <h3>via Keybase</h3>
              <p>
                If you're using Keybase, you can just pull my key with following command:
              </p>
              <CopyableText content="keybase pgp pull chez14" />
            </div>


            <div className="my-5">
              <h2>Verify the GPG Keys</h2>
              <p>Any of those step should be able to properly grab my public keys with following details:</p>

              <CopyableText content="gpg --fingerprint --list-keys 332d7eafbcdb79a3" />

              <pre className="bg-black p-3">{`pub   rsa4096 2018-07-07 [SC] [expires: 2040-07-01]
      4F7A 34FF 4BD5 630B F618  BC06 332D 7EAF BCDB 79A3
uid           [ultimate] Gunawan Christianto (chez14) <[redacted-to-prevent-spam]>
sub   rsa4096 2018-07-07 [E] [expires: 2040-07-01]
sub   rsa4096 2018-11-19 [S] [expires: 2040-07-01]`}</pre>
              <p>
                If you're using Keybase, please note that there might be some slight differences on the identity part. Additionally,
                the email has been stripped to prevent scrapers sending stuffs.
              </p>
            </div>

            <div className="mt-5">Thank you!</div>
          </Col>
        </Row>
      </Container>
    </>
  )
}

export default PgpPage
