import { Metadata } from 'next';
import { Alert, AlertHeading, Col, Container, Row } from 'react-bootstrap';
import { NavbarContext } from '~/components/navbar/navbar-context';
import IssueContent from './_fragments/issue-content';

export const metadata: Metadata = {
  title: 'Works',
  description: "Chris's works and projects over the years."
}

export const revalidate = 3600;

async function WorksPage() {
  return (
    <>
      <NavbarContext routes='/works' bg='dark' logoVariant='dark' />

      <Container>
        <div className="my-3">
          <h1>Works and Projects</h1>
        </div>
      </Container>

      <Container>
        <div className="my-5">
          <Row>
            <Col xs={12} md={8}>
              <Alert variant='info'>
                <AlertHeading>Under Construction</AlertHeading>
                <p>
                  This page still WIP, track the development on
                  Issue <a href="https://gitlab.com/chez14/christianto.net/-/issues/12" target="_blank" rel="noopener noreferrer">#12</a>.
                  For now, please enjoy the following list.
                </p>
              </Alert>
            </Col>
          </Row>
        </div>

        <div className="my-5">
          <Row>
            <Col xs={12} md={8}>
              <IssueContent />
            </Col>
          </Row>
        </div>

      </Container>
    </>
  )
}

export default WorksPage
