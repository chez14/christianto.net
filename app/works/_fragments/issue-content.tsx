import { Gitlab } from '@gitbeaker/browser';
import { remark } from 'remark';
import remarkHtml from 'remark-html';

async function getIssueContent() {
  let projectId = 24281566;
  let issueIid = 11;

  let gl = (new Gitlab({}));
  let issueContent = await gl.Issues.show(projectId, issueIid);

  let processedContent = await remark()
    .use(remarkHtml)
    .process(issueContent.description);

  return processedContent.toString();
}

async function IssueContent() {
  const issueContent = await getIssueContent()
  return (
    <div dangerouslySetInnerHTML={{ __html: issueContent }}></div>
  )
}

export default IssueContent
