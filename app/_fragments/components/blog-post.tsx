"use client";

import { faObjectGroup } from '@fortawesome/free-regular-svg-icons';
import { faHashtag } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classNames from 'classnames';
import Link from 'next/link';
import { Badge, Card } from 'react-bootstrap';
import { When } from 'react-if';
import useProgressiveImage from '~/components/utils/use-progressive-image';
import { BlogPost as BlogPostModel } from '~/data/blogs/blog-post';

import styles from './blog-post.module.scss';

export interface BlogPostProps {
  post: BlogPostModel
}

function BlogPost({ post }: BlogPostProps) {

  let postDate = new Date(post.date);
  let blogFeaturedImage = useProgressiveImage(post.featuredImage?.source_url);

  return (
    <Card className={styles.postItem}>
      <When condition={!!post.featuredImage?.source_url}>
        <Link className="text-decoration-none text-light" href={post.link}>
          <span className={classNames({ "post-image-container": true, "is-visible": !!blogFeaturedImage })} style={{ "--blog-featured-img": `url('${blogFeaturedImage}')` }} />
        </Link>
      </When>
      <Card.Body className={classNames({ 'post-imageless': !post.featuredImage?.source_url })}>

        {/* Post title */}
        <Link className="text-decoration-none text-light" href={post.link}>
          <Card.Title as="div" className='h4' dangerouslySetInnerHTML={{ __html: post.title }} />
        </Link>

        <Card.Text as="div" className='small my-2'>
          {postDate.toLocaleString('en', { dateStyle: "full", timeStyle: 'short' })}
        </Card.Text>

        {/* Mini meta data listers */}
        <Card.Text as="div" className="post-taglists small">
          {/* Category lister */}
          {post.categories.map((ca) => <Link href={ca.link} target="_blank" rel="noopener noreferrer" key={ca.link}>
            <Badge bg='dark'><FontAwesomeIcon icon={faObjectGroup} /> {ca.name}</Badge>
          </Link>)}

          {/* tags Lister */}
          {post.tags.map((ca) => <Link href={ca.link} target="_blank" rel="noopener noreferrer" key={ca.link}>
            <Badge bg='dark'><FontAwesomeIcon icon={faHashtag} /> {ca.name}</Badge>
          </Link>)}
        </Card.Text>

        <When condition={!post.featuredImage?.source_url}>
          {/* Post Excercept */}
          <Card.Text as="div" dangerouslySetInnerHTML={{ __html: post.excerpt }} />
        </When>
      </Card.Body>
    </Card>
  )
}

export default BlogPost
