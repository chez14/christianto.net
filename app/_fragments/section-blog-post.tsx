import { faArrowRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import classNames from 'classnames'
import Link from 'next/link'
import { Button, Col, Row } from 'react-bootstrap'
import SectionHead from '~/components/section-head/section-head'
import { BlogPost } from '~/data/blogs/blog-post'
import BlogPostItem from './components/blog-post'
import BlogPostFeaturedPost from './components/featured-blog'
export interface HomeSectionBlogPost {
  blogs: BlogPost[]
}

function HomeSectionBlogPost({ blogs }: HomeSectionBlogPost) {

  let [firstBlog, ...otherPosts] = blogs;

  return (
    <>
      <SectionHead title="Blog Post">
        <Link href="https://blog.christianto.net/?utm_source=home&utm_medium=section" passHref legacyBehavior target="_blank" rel="noreferrer noopener">
          <Button variant="dark">
            All Blogs <FontAwesomeIcon icon={faArrowRight} />
          </Button>
        </Link>
      </SectionHead>

      <div className={classNames(["my-5"])}>
        <BlogPostFeaturedPost post={firstBlog} />
      </div>
      <div className="my-5">
        <Row className='row-cols-1 row-cols-md-2 row-cols-lg-3 g-4'>
          {otherPosts.map((post) => <Col key={post.id}><BlogPostItem post={post} /></Col>)}
        </Row>
      </div>
    </>
  )
}

export default HomeSectionBlogPost
