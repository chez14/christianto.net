"use client";

import { Turnstile } from '@marsidev/react-turnstile';
import { AxiosError } from 'axios';
import React, { useRef, useState } from 'react';
import { Alert, Button, Form, Spinner } from 'react-bootstrap';
import { When } from 'react-if';
import ExternalLink from '~/components/utils/external-link';
import { frontendApiCaller } from '~/util/api';

export interface ContactUiMessage {
  status: "info" | "danger",
  message: string
}


function ContactPageContactForms() {
  const catpchaRef = useRef();
  const [isSending, setIsSending] = useState(false)
  const [isLoading, setIsLoading] = useState(true)
  const [showForm, setShowForm] = useState(true)
  const [uiMessage, setUiMessage] = useState<ContactUiMessage | undefined>(undefined)

  // TODO: Add status for loaders (i.e "Waiting for captcha verification...");

  async function onSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();

    // TODO: Reflow all of these mess.
    setIsSending(true);

    // Clear the UI message.
    setUiMessage(undefined);
    let formData = new FormData(e.currentTarget);
    try {
      await frontendApiCaller.post("contact", Object.fromEntries(formData));
      setUiMessage({
        status: "info",
        message: "Thank you for contacting me, your message has been received."
      });
      setShowForm(false);
    } catch (e) {
      let errorStatus = "There has been an unknown error while sending your message, please try again, or contact me directly via email if the issue still persists.";
      if (e instanceof AxiosError) {
        errorStatus = "There has been an error while sending your message, please check your network and try again.";
        console.log(e.response?.data);
        if (e.response?.data) {
          errorStatus = `There has been an error while sending your message: ${e.response?.data?.message}`
        }

        // 5xx error should be prioritized. Hence i put it here, to make sure
        // this error one is not overriden.
        if (e.response && (Math.floor(e.response.status / 100) === 5)) {
          errorStatus = `There has been an internal error while sending your message, please contact me directly via email.`
        }
      }

      setUiMessage({
        status: "danger",
        message: errorStatus
      });
      // Allow form resubmission.
      (catpchaRef.current as any).reset();
    }
    setIsSending(false);
  }

  return (
    <>
      <When condition={uiMessage !== undefined}>
        <Alert variant={uiMessage?.status}>
          {uiMessage?.message}
        </Alert>
      </When>
      <When condition={showForm}>
        <Form onSubmit={(e) => onSubmit(e)}>
          <Form.Group className="mb-3" controlId="contactName">
            <Form.Label>Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Firstname Middlename Lastname"
              name="name"
              disabled={isSending}
              maxLength={64}
              required
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="contactEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="email-name@domain.tld"
              name="email"
              disabled={isSending}
              maxLength={128}
              required
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="messageSubject">
            <Form.Label>Subject</Form.Label>
            <Form.Control
              type="text"
              placeholder="Mesage Title"
              name="subject"
              disabled={isSending}
              maxLength={128}
              required
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="messageContent">
            <Form.Label>Message</Form.Label>
            <Form.Control
              as="textarea"
              placeholder="Type your inquiries"
              name="message"
              disabled={isSending}
              style={{ height: '10rem' }}
              minLength={20} maxLength={5000}
              required
            />
            <Form.Text className="text-muted">
              For security reason, no formatting is supported, use <ExternalLink
                href="https://daringfireball.net/projects/markdown/syntax">markdown syntax</ExternalLink> if
              you want some kind of formatting.
            </Form.Text>
          </Form.Group>

          <Form.Group className='mb-3' controlId='submit' style={{ minHeight: 71 }}>
            <Turnstile
              siteKey={process.env.NEXT_PUBLIC_TURNSTILE_SITE_KEY || ''}
              options={{
                theme: "dark",
                responseFieldName: 'captcha'
              }}
              ref={catpchaRef}
              onSuccess={() => setIsLoading(false)}
              onError={() => {
                setUiMessage({
                  status: "danger", message: "Captcha error, please try again."
                });
              }}
            />
          </Form.Group>

          <Form.Group className='mb-3' controlId='submit'>
            <Button type='submit' disabled={isSending || isLoading}>
              Send Contact <When condition={isSending}><Spinner as="span" animation="border" size="sm" role="status" aria-hidden="true" /></When>
            </Button>
          </Form.Group>
        </Form>
      </When>
    </>
  )
}

export default ContactPageContactForms
