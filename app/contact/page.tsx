import { Metadata } from 'next'
import Link from 'next/link'
import { Col, Container, Row } from 'react-bootstrap'
import profilePict from "~/assets/home/profile.jpg"
import { NavbarContext } from '~/components/navbar/navbar-context'
import { getUrlBaseName } from '~/util/url-helper'
import ContactPageContactForms from './_fragments/contact-form'

let urlBase = getUrlBaseName();

export const metadata: Metadata = {
  title: "Contact Chris",
  description: "Have something to discuss with Chris? Contact him here!",
  openGraph: {
    images: [
      {
        url: `${urlBase.origin}${profilePict.src}`,
        width: profilePict.width,
        height: profilePict.height,
        alt: "An image of Chris in anime style"
      }
    ]
  }
}

function ContactPage() {
  return (
    <>
      <NavbarContext addPadders bg='dark' logoVariant='dark' />

      <div className="my-3">
        <Container>
          <h1>Contact Me</h1>
        </Container>
      </div>

      <Container>
        <div className="my-5">
          <Row>
            <Col xs={12} md={6}>
              <h2>Fill up a form, and I&apos;ll send you a reply</h2>

              <div className='mb-5'>
                <ContactPageContactForms />
              </div>

            </Col>
            <Col xs={12} md={{ offset: 2, span: 4 }}>
              <h2>Other contact?</h2>
              <p>
                Email: <code>hi (at) this domain</code> (<Link href="/pgp">PGP Key</Link>). <br />
                Or find me <Link href="/links">anywhere</Link>.
              </p>
            </Col>
          </Row>
        </div>
      </Container>
    </>
  )
}

export default ContactPage

