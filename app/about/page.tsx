import { Metadata } from 'next'
import { NavbarContext } from '~/components/navbar/navbar-context'
import AboutPageContent1AboutMe from './_fragments/content-1-about-me'
import AboutPageMainBanner from './_fragments/main-banner'
import styles from "./about.module.scss"

export const metadata: Metadata = {
  title: "About Chris",
  description: "Chris is a software engineer in Bandung area. Chris makes website, optimize them, and insert text to places like this :). Learn more about Chris in this page!",
  openGraph: {
    type: 'profile',
    firstName: "Gunawan",
    lastName: "Christianto",
    username: "chez14",
    gender: "male"
  }
}

function AboutPage() {
  return (
    <>
      <NavbarContext logoVariant='mono' variant='light' className={styles.navbar} routes='/about' />

      <AboutPageMainBanner />

      <div className="my-5">
        <AboutPageContent1AboutMe />
      </div>
    </>
  )
}

export default AboutPage
