import { Col, Container, Row } from 'react-bootstrap'
import ExternalLink from '~/components/utils/external-link'

function AboutPageContent1AboutMe() {
  return (
    <div className='my-5'>
      <Container>
        <Row>
          <Col xs={12} md={8}>

            <div>
              <h2 className='display-3'>Henlo, I'm <span className="text-accent-a">Chris</span>.</h2>
              <p className="fs-3">
                I'm a software engineer based in Indonesia.
              </p>
              <p>
                I help our clients with their requests and needs, translate it to technical requirements, and
                manage engineers to work on them. In addition, I manage the company's infrastructure as well.
              </p>

              <p>
                I have a weird hobby of checking many platform's API, learn how they implement the limiters, safe guards and more, then
                try it out by my self by running some weird, specific, and sometimes dumb experiments. This weird hobby
                of mine actually helps me to advise my clients when they want use said platform. One of the latest examples
                is how I helped a Property Management company run their backup pipeline for their directory app.
                I helped by making the reports, documentations and proposing the restore strategy. Not exactly web development
                related, right? But that's why I pick the Software Engineer title, because I love to program other stuff too!
              </p>

            </div>

            <div className='my-5'>
              <h2>Skillsets</h2>

              <Row>
                <Col xs={12} md={4}>
                  <h4>Front-end</h4>
                  <ul>
                    <li>HTML, CSS, Js, SASS</li>
                    <li>
                      <ExternalLink href="https://react.dev/">React</ExternalLink>&nbsp;
                      (<ExternalLink href="https://www.typescriptlang.org/">TypeScript</ExternalLink>)
                    </li>
                    <li>
                      <ExternalLink href="https://angular.io/">Angular</ExternalLink></li>
                    <li className='text-light text-opacity-50'>
                      <ExternalLink href="https://jquery.com/">jQuery</ExternalLink>
                    </li>
                    <li className='text-light text-opacity-50'>
                      <ExternalLink href="https://getbootstrap.com/">Bootstrap</ExternalLink>
                    </li>
                  </ul>
                </Col>
                <Col xs={12} md={4}>
                  <h4>Back-end</h4>
                  <ul>
                    <li>
                      <ExternalLink href="https://laravel.com/">Laravel</ExternalLink>&nbsp;
                      (+ <ExternalLink href="https://inertiajs.com/">IntertiaJs</ExternalLink>)
                    </li>
                    <li><ExternalLink href="https://nextjs.org/">NextJs</ExternalLink></li>
                    <li><ExternalLink href="https://astro.build/">Astro.build</ExternalLink></li>
                    <li><ExternalLink href="https://www.codeigniter.com/">CodeIgniter 3+4</ExternalLink> (PHP)</li>
                    <li><ExternalLink href="https://fatfreeframework.com/">FatFree Framework 3.6+</ExternalLink> (PHP)</li>
                    <li><ExternalLink href="https://expressjs.com/">Express</ExternalLink> (Js/Ts)</li>
                    <li><ExternalLink href="https://github.com/tokio-rs/axum">Axum</ExternalLink> (Rust) — Learning</li>
                    <li>
                      <ExternalLink href="https://www.mysql.com/">MySQL</ExternalLink>, &nbsp;
                      <ExternalLink href="https://mariadb.org/">MariaDB</ExternalLink>, &nbsp;
                      <ExternalLink href="https://www.mongodb.com/">MongoDB</ExternalLink>.
                    </li>
                  </ul>
                </Col>
                <Col xs={12} md={4}>
                  <h4>Other</h4>
                  <p>...but web-tech related.</p>
                  <ul>
                    <li><ExternalLink href="https://bun.sh/">Bun</ExternalLink></li>
                    <li><ExternalLink href="https://wordpress.org/">WordPress</ExternalLink></li>
                    <li>
                      <ExternalLink href="https://httpd.apache.org/">Apache</ExternalLink>,&nbsp;
                      <ExternalLink href="https://www.nginx.com/">Nginx</ExternalLink>
                    </li>
                  </ul>
                </Col>

              </Row>
              <Row>
                <Col xs={12} md={6}>
                  <h4>Other</h4>
                  <p>...but for anything else.</p>
                  <ul>
                    <li><ExternalLink href="https://www.rust-lang.org/">Rust</ExternalLink> — learning</li>
                    <li>
                      <ExternalLink href="https://grafana.com/">Grafana</ExternalLink>,&nbsp;
                      <ExternalLink href="https://prometheus.io/">Prometheus</ExternalLink>
                    </li>
                    <li><ExternalLink href="https://www.docker.com/">Docker</ExternalLink></li>
                    <li><ExternalLink href="https://docs.gitlab.com/ee/ci/">GitLab CI/CD</ExternalLink></li>
                    <li>
                      <ExternalLink href="https://www.digitalocean.com/">DigitalOcean</ExternalLink>,&nbsp;
                      <ExternalLink href="https://linode.com/">Linode</ExternalLink>
                      </li>
                    <li><ExternalLink href="https://cloudflare.com/">Cloudflare</ExternalLink></li>
                    <li><ExternalLink href="https://github.com/octodns/octodns">octoDNS</ExternalLink></li>
                  </ul>
                </Col>
                <Col xs={12} md={6}>
                  <h4>OSes</h4>
                  <p>
                    Since some job descriptions sometime mention certain experience requirements with certain OS,
                    I'm adding this extra section.
                  </p>
                  <ul>
                    <li><ExternalLink href="https://ubuntu.com/">Ubuntu</ExternalLink> (my current workstation)</li>
                    <li><ExternalLink href="https://www.microsoft.com/windows">Windows</ExternalLink></li>
                    <li><ExternalLink href="https://hub.docker.com/_/alpine">Alpine Linux</ExternalLink> (docker image only)</li>
                  </ul>
                </Col>
              </Row>
            </div>

            <div className="my-5">
              <h3>Languages</h3>
              <ul>
                <li>
                  <p className="fw-bold m-0">Bahasa Indonesia</p>
                  <p>Native level</p>
                </li>
                <li>
                  <p className="fw-bold m-0">English</p>
                  <p>Business level</p>
                </li>
                <li>
                  <p className="fw-bold m-0">日本語</p>
                  <p>Just strarted to learn ✨</p>
                </li>
              </ul>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default AboutPageContent1AboutMe
