import React from 'react'
import { Container } from 'react-bootstrap';

import styles from "./main-banner.module.scss";

function AboutPageMainBanner() {
  return (
    <div className={styles.mainBanner}>
      <Container>
        <h1>About Me</h1>
      </Container>
    </div>
  )
}

export default AboutPageMainBanner
