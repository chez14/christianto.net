import { compile } from 'pug';
import isEmail from 'validator/lib/isEmail';
import { smtpTransport } from '~/util/server/email';
import { validateTurnstileCaptcha } from '~/util/server/turnstile-api';

import template from "~/data/email-template/contact.pug";
const contactTemplate = compile(template);

function buildReponse(status: number, body: object) {
  return new Response(JSON.stringify(body), { status });
}

export async function POST(
  request: Request
) {
  let { name, email, subject, message, captcha } = await request.json();

  if (request.method != "POST") {
    return buildReponse(400, { status: false, message: "Not found." });
  }

  if (!captcha) {
    return buildReponse(400, { status: false, message: "Captcha needs to be solved." });
  }

  if (!isEmail(email)) {
    return buildReponse(400, { status: false, message: "Emails needs to be a valid email." });
  }

  if (!name || !subject || !message) {
    return buildReponse(400, { status: false, message: "All required form fields need to be filled." });
  }

  if (message.length < 20) {
    return buildReponse(400, { status: false, message: "Message too short (minimum 20 chars)." });
  }

  if (message.length > 5000) {
    return buildReponse(416, { status: false, message: "Message too long (maximum 5000 chars)." });
  }

  // check the catpcha
  if (!await validateTurnstileCaptcha(captcha)) {
    return buildReponse(400, { status: false, message: "Invalid catpcha." });
  }

  let messageContent = contactTemplate({
    name,
    email,
    subject,
    message
  });

  await smtpTransport.sendMail({
    to: process.env.NEXT_CONTACT_EMAIL,
    from: process.env.NEXT_SMTP_FROM,
    replyTo: email,
    subject: `[Christianto.net] Contact Form: ${subject}`,
    html: messageContent
  });

  return buildReponse(201, { status: true, message: "Message sent." });
}
