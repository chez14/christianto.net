import { Col, Container, Row } from 'react-bootstrap'

function LinksPageContent1() {
  return (
    <>
      <Container>
        <Row>
          <Col xs={12} md={8}>
            <h2>Find Me Everywhere</h2>
            <p className="lead">
              Feeling déjà vu? That account might have been be me!
              Connect with me, or see my solution catalog, maybe there's something that fits your need.
            </p>
          </Col>
        </Row>
      </Container>
    </>
  )
}

export default LinksPageContent1
