import Link from 'next/link'
import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import LinkRepeater from '~/components/link-repeater/link-repeater'
import { linksDeveloper, linksOthers, linksPinned, linksSocials, linksStores } from '~/data/contact/linksies'

function LinkPageContent2() {
  return (
    <>
      <Container>
        <div className="my-5">
          <LinkRepeater links={[...linksPinned, ...linksDeveloper]} />
        </div>

        <div className="my-5">
          <h3>Package Store</h3>
          <LinkRepeater links={linksStores} />
        </div>

        <div className="my-5">
          <h3>Socials</h3>
          <LinkRepeater links={linksSocials} />
        </div>

        <div className="my-5">
          <h3>Others</h3>
          <LinkRepeater links={linksOthers} />
        </div>

        <div className="my-5">
          <Row>
            <Col xs={12} md={8}>
              <h3>Validate</h3>
              <p>
                This list is not updated regularly, but you can always ask for proof of ownership.
                I'll send you a PGP-Signed text containing the profile link or id that you can validate via:
              </p>
              <ul>
                <li>
                  <span className='fw-bolder d-block'>Keybase</span>
                  with account id <b>@chez14</b>, <a href="https://keybase.io/verify" target="_blank" rel="noreferrer noopener">verify here</a>.
                </li>
                <li>
                  <span className='fw-bolder d-block'>PGP Key</span>
                  signed by key id <code>4F7A34FF4BD5630BF618BC06332D7EAFBCDB79A3</code>
                  , <Link href="/pgp">grab the pubkey here</Link>.
                </li>
              </ul>
            </Col>
          </Row>
        </div>
      </Container>
    </>
  )
}

export default LinkPageContent2
