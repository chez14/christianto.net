import { Metadata } from 'next';
import { Container } from 'react-bootstrap';
import profilePict from "~/assets/home/profile.jpg";
import CtaContact from '~/components/ctas/banner-cta-contact';
import { getUrlBaseName } from '~/util/url-helper';
import LinksPageContent1 from './_fragments/content-1-intro';
import LinkPageContent2 from './_fragments/content-2-linkies';
import LinksPageMainBanner from './_fragments/main-banner';

import { NavbarContext } from '~/components/navbar/navbar-context';
import styles from "./links.module.scss";

let urlBase = getUrlBaseName();

export const metadata: Metadata = {
  title: "Links",
  description: "Find Chris everywhere anywhere! Connect with him to see what he's being up to.",
  openGraph: {
    images: [
      {
        url: `${urlBase.origin}${profilePict.src}`,
        width: profilePict.width,
        height: profilePict.height,
        alt: "An image of Chris in anime style"
      }
    ]
  }
}

function LinksPage() {
  return (
    <>
      <NavbarContext bg='dark' logoVariant='mono' variant='dark' className={styles.navbar}/>

      <LinksPageMainBanner />

      <div className="my-5">
        <LinksPageContent1 />
      </div>

      <div className="my-5">
        <LinkPageContent2 />
      </div>

      <div className="my-5">
        <Container>
          <CtaContact title='Just want to contact me privately?' description="Fill up this contact form instead." />
        </Container>
      </div>
    </>
  )
}

export default LinksPage