import { Metadata } from 'next';
import { Container } from 'react-bootstrap';
import { NavbarContext } from '~/components/navbar/navbar-context';
import OssContributionContent from './_fragments/content';
import OssContributionHero from './_fragments/hero';

export const metadata: Metadata = {
  title: "Open Source Contributions",
  description: "See most of chez14's Open Source contributions here!",
}

function OssContributionsPage() {
  return (
    <>
      <NavbarContext addPadders bg='dark' logoVariant='dark' />

      <Container>
        <OssContributionHero />

        <OssContributionContent />
      </Container>
    </>
  )
}

export default OssContributionsPage