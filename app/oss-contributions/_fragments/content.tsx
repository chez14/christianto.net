"use client";

import { Button, Spinner } from 'react-bootstrap'
import { When } from 'react-if'
import { Contribution } from '~/data/api/oss-contributions'
import ContributionGroup from './contrib/group'

import { useInfiniteQuery } from '@tanstack/react-query'
import { useCallback, useEffect, useMemo, useRef } from 'react'
import { useInViewport } from 'react-in-viewport'
import { GeneralPaginatedApiResponse } from '~/data/api/generic-response'
import { portmafiaApi } from '~/util/api'

type GroupedContribution = {
  year: string,
  contributions: Contribution[]
}

const fetchContributions = async ({ pageParam }: any) => {
  return (await portmafiaApi.get<GeneralPaginatedApiResponse<Contribution>>('repo-contribution', {
    params: { cursor: pageParam }
  })).data;
}


function OssContributionContent() {
  const loaderRef = useRef<HTMLDivElement>(null);

  const { inViewport } = useInViewport(loaderRef);

  const {
    data,
    fetchNextPage,
    hasNextPage,
    isFetching,
  } = useInfiniteQuery({
    queryKey: ['oss-contributions'],
    queryFn: fetchContributions,
    initialPageParam: undefined,
    getNextPageParam: (lastPage) => lastPage.meta.next_cursor,
  });

  const contributions = useMemo<GroupedContribution[]>(() => {
    const group: { [key: string]: Contribution[] } = {};
    data?.pages.forEach(resp => {
      resp.data?.forEach((contribution) => {
        const year = contribution.created_at.substring(0, 4);
        group[year] ||= [];
        group[year].push(contribution);
      });
    })

    return Object.keys(group)
      .sort()
      .reverse()
      .map<GroupedContribution>(year => ({
        year,
        contributions: group[year].sort((a, b) => b.type.localeCompare(a.type))
      }));
  }, [data])

  const handleLoadMore = useCallback(
    () => {
      if (hasNextPage) {
        fetchNextPage();
      }
    },
    [hasNextPage, fetchNextPage],
  )

  useEffect(() => {
    if (inViewport) {
      handleLoadMore()
    }
    return () => { }
  }, [inViewport, handleLoadMore])


  return (
    <>
      {contributions.map(group => <div key={group.year} className='my-5'>
        <ContributionGroup title={group.year} contributions={group.contributions} />
      </div>)}

      <When condition={isFetching || !data}>
        <div className="text-center py-3">
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        </div>
      </When>

      <div className='text-center py-3' ref={loaderRef}>
        <When condition={hasNextPage}>
          <Button size='lg' variant='secondary' className='w-100' onClick={handleLoadMore}>Load More</Button>
        </When>
      </div>
    </>
  )
}

export default OssContributionContent
