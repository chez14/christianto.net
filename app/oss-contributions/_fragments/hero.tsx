import React from 'react'
import { Col, Row } from 'react-bootstrap'

function OssContributionHero() {
  return (
    <>
      <Row>
        <Col xs={12} md={8}>
          <h1>Open Source Contributions</h1>

          <p>
            On this page, you can find a record of some of my contributions to open
            source projects over the years. The listed contributions include
            both issues and merge requests, which are sometimes referred to as
            pull requests. While I may have made other contributions, such as
            publishing new projects or providing answers on Discord or community
            posts, these types of contributions are not currently tracked due to
            the complexity of automation.
          </p>

          <p>
            The contributions are organized by year and further sorted by type,
            and the creation date of issues/merge requests for
            convenience.
          </p>
        </Col>
      </Row>
    </>
  )
}

export default OssContributionHero
