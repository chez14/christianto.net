import { faFile } from '@fortawesome/free-regular-svg-icons';
import { faCodePullRequest } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classNames from 'classnames';
import { Badge } from 'react-bootstrap';
import { When } from 'react-if';

import styles from "./item-label.module.scss";

export interface ItemLabelProps {
  type: 'issue' | 'mr'
  status?: 'open' | 'close' | 'merged'
}

const statusCaptionDictionary: { [key: string]: string } = {
  open: "opened",
  close: "closed",
  merge: "merged",
}

function ItemLabel({ type, status }: ItemLabelProps) {
  let color = styles.statusOpen;
  let statusCaption = "";
  let icon = faCodePullRequest;
  if (type === 'issue') {
    icon = faFile
  }

  if (status) {
    if (status === 'close') {
      color = styles.statusClose;
    } else if (status === 'merged') {
      color = styles.statusMerged;
    }
    statusCaption = statusCaptionDictionary[status];
  }

  return (
    <Badge className={classNames(styles.label, color)} bg='transparent'>
      <span className={classNames(styles.opaque, styles.icon)}><FontAwesomeIcon icon={icon} /></span>
      <When condition={status}>
        <span>{statusCaption}</span>
      </When>
    </Badge>
  )
}

export default ItemLabel
