import classNames from 'classnames'
import { Col, Row } from 'react-bootstrap'
import { Contribution } from '~/data/api/oss-contributions'
import ContributionItem from './item'

import styles from "./group.module.scss"

export interface ContributionGroupProps {
  title: string,
  contributions: Contribution[]
}

function ContributionGroup({ title, contributions }: ContributionGroupProps) {
  return (
    <div>
      <h2 className='mb-3'>{title}</h2>

      <Row className={classNames(styles.grouping, "g-3 align-items-stretch")}>
        {contributions.map(contribution => <Col key={contribution.id}>
          <ContributionItem contribution={contribution} className='h-100' />
        </Col>)}
      </Row>
    </div>
  )
}

export default ContributionGroup
