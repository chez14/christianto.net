import { HTMLAttributes, useMemo } from 'react';
import { Card } from 'react-bootstrap';

import classNames from 'classnames';
import { Contribution } from '~/data/api/oss-contributions';
import ItemLabel from './item-label';
import styles from "./item.module.scss";

export interface ContributionItemProps extends Pick<HTMLAttributes<HTMLElement>, 'className'> {
  contribution: Contribution,
}

const dateFormatter = new Intl.DateTimeFormat('en-US', {
  dateStyle: 'long',
});

function ContributionItem({ contribution, className }: ContributionItemProps) {
  const parsedDate = useMemo(() => new Date(contribution.created_at), [contribution.created_at]);

  return (
    <Card className={classNames(styles.card, className)}>
      <Card.Body>
        <div className="item-header">
          <div className='item-header__repo'>
            <a href={contribution.repo.url} target="_blank" rel='noopener noreferrer'>{contribution.repo.name}</a>
          </div>
          <div className='item-header__label'>
            <ItemLabel type={contribution.type} />
          </div>
        </div>
        <div className="item-title">
          <p>
            <a href={contribution.url} target="_blank" rel='noopener noreferrer'>{contribution.title} (#{contribution.number})</a>
          </p>
        </div>
        <div className="item-timestamp">
          <p className={styles.timestamp}>{dateFormatter.format(parsedDate)}</p>
        </div>
      </Card.Body>
    </Card>
  )
}

export default ContributionItem
