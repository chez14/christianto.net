# Christianto.net — The Landing Page

This repo hosts the main source code of Chris' Landing Page.

There's no ETA or dev roadmap for now. All code are open for everyone to see and
comment to.

**Resources**

- Live \
  https://christianto.net

- Plong theme \
  https://ch14.me/plong

- Portmafia API \
  https://gitlab.com/net.christianto/portmafia-api

- Find me everywhere \
  https://christianto.net/links

## Getting Started

Make sure you have following tech stack:
- Vanilla WP Blog (for Portmafia) \
  https://wordpress.org/download/

- Portmafia Instance (this site's backend) \
  https://gitlab.com/net.christianto/portmafia-api

- NodeJS + Yarn \
  https://nodejs.org/en/ \
  https://yarnpkg.com/getting-started/install

To start the development environment:
1. Copy `.env` file to `.env.local`, then fill up the configuration according to
    your local environment configuration. You might need to create a Google
    Analytics account first to use several functions.

2. Start the Portmafia API repo and your WP Instance. \
    Check each project instruction for detailed explanation.

3. Start the dev server
   ```
   yarn run dev
   ```

Learn more about environment variable:
https://nextjs.org/docs/basic-features/environment-variables.

## License

[MIT](./LICENSE). Feel free to use this project for non-evil purposes!
