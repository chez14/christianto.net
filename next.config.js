/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'secure.gravatar.com',
        pathname: '/avatar/**',
      },
    ],
  },
  webpack: (config, options) => {
    config.module.rules.push({
      test: /\.(pug)$/i,
      use: [
        {
          loader: 'raw-loader'
        },
      ],
    });

    return config
  },
}

module.exports = nextConfig
