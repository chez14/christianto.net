export interface GeneralApiResponse<T = any> {
  status: boolean,
  data?: T,
  message?: string
}

export interface GeneralPaginatedApiResponse<T = any> extends GeneralApiResponse<T[]> {
  links: {
    first: string | null,
    last: string | null,
    prev: string | null,
    next: string | null
  },
  meta: {
    path: string,
    per_page: number,
    next_cursor: string | null,
    prev_cursor: string | null
  },
}
