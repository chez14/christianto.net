
export interface Contribution {
  id: string,
  repo: Repo,
  title: string,
  number: string,
  url: string,
  type: 'issue' | 'mr',
  created_at: string,
}

export interface Repo {
  id: string,
  name: string,
  url: string
}

