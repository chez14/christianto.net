import { faGithub, faGitlab, faKeybase, faLinkedin, faMastodon, faStackOverflow, faTwitch, faTwitter, faWordpressSimple } from "@fortawesome/free-brands-svg-icons";
import { faLink } from "@fortawesome/free-solid-svg-icons";

export interface LinkItem {
  icon: any,
  title: string,
  link: string
}

export const linksPinned: LinkItem[] = [
  { title: "Blog", icon: faWordpressSimple, link: "https://blog.christianto.net/" },
  { title: "Notes", icon: faWordpressSimple, link: "https://notes.christianto.net/" },
  { title: "LinkedIn", icon: faLinkedin, link: "https://www.linkedin.com/in/chez14/" }
];

export const linksDeveloper: LinkItem[] = [
  { title: "GitLab", icon: faGitlab, link: "https://gitlab.com/chez14" },
  { title: "GitHub", icon: faGithub, link: "https://github.com/chez14" },
  { title: "StackOverflow", icon: faStackOverflow, link: "https://stackoverflow.com/users/4721245/chris-qiang" },
  { title: "Crowdin", icon: faLink, link: "https://crowdin.com/profile/chez14" },
];

export const linksSocials: LinkItem[] = [
  { title: "Twitter", icon: faTwitter, link: "https://twitter.com/heychez14" },
  { title: "Keybase", icon: faKeybase, link: "https://keybase.io/chez14" },
  { title: "Mastodon.online", icon: faMastodon, link: "https://mastodon.online/@chez14" },
  // { title: "Cohost", icon: faLink, link: "https://cohost.org/chez14" },
];

export const linksStores: LinkItem[] = [
  { title: "Packagist", icon: faLink, link: "https://packagist.org/users/chez14/" },
  { title: "NPM", icon: faLink, link: "https://www.npmjs.com/~chez14" },
  { title: "Docker", icon: faLink, link: "https://hub.docker.com/u/chez14" },
  { title: "Visual Studio Marketplace", icon: faLink, link: "https://marketplace.visualstudio.com/publishers/chez14" },
  { title: "Amazon ECR (soon™)", icon: faLink, link: "https://gallery.ecr.aws/chez14" },
];

export const linksOthers: LinkItem[] = [
  { title: "LinkedIn", icon: faLinkedin, link: "https://www.linkedin.com/in/chez14/" },
  { title: "Twitch", icon: faTwitch, link: "https://twitch.tv/chez14" },
  // { title: "YouTube", icon: faYoutube, link: "https://youtube.com/@chez14" },
  // { title: "Unsplash", icon: faUnsplash, link: "https://unsplash.com/@chez14" },
];
