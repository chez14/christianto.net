import { knownInfra, knownJsLibraries, knownLanguages, knownRustLibraries, WorkItem } from "./model/works";

export let workList: WorkItem[] = [
  {
    title: "Snippy",
    slug: "snippy",
    repoUrl: {
      repoType: "gitlab",
      url: "https://gitlab.com/net.christianto/snippy",
      kind: "repo"
    },
    tags: ["weird", "gitlab-api", "snippets"],
    techStack: [
      knownLanguages['rust']
    ]
  }, {
    title: "Twitch VTubers SEA API",
    slug: "twich-vtuber-sea-api",
    repoUrl: {
      repoType: "gitlab",
      url: "https://gitlab.com/net.christianto/twich-vtuber-sea-api",
      kind: "repo"
    },
    tags: ["vtubers", "api"],
    techStack: [
      knownLanguages['rust'],
      knownRustLibraries['axum']
    ]
  }, {
    title: "Publisher",
    slug: "publisher",
    repoUrl: {
      repoType: "gitlab",
      url: "https://gitlab.com/chez14/publisher",
      kind: "repo"
    },
    tags: ["publish", "docker", "gitlab-ci", "autotag"],
    techStack: [
      knownLanguages['bash'],
      knownInfra['docker']
    ]
  }, {
    title: "Oxam",
    slug: "oxam",
    description: "Oxam is Examination Management App, made for Laboratorium Komputer of Department of Informatics, Faculty of Information Technology and Sciences.",
    repoUrl: {
      repoType: "gitlab",
      url: "https://gitlab.com/ftis-admin/oxam",
      kind: "repo"
    },
    tags: ["parahyangan", "examination", "plotter", "LDAP"],
    techStack: [
      knownLanguages['php'],
      knownLanguages['js'],
      knownLanguages['bat'],
      knownJsLibraries['react'],
      knownJsLibraries['mobx'],
      { title: "LDAP", type: "protocol" },
    ]
  }, {
    title: "F3-Ilgar",
    slug: "f3-ilgar",
    description: "A simple migration tool for Fat-Free Framework",
    repoUrl: {
      repoType: "gitlab",
      url: "https://gitlab.com/chez14/f3-ilgar/-/tree/next",
      kind: "repo"
    },
    tags: ["migration", "fatfreeframework"],
    techStack: [
      knownLanguages['php'],
    ]
  }, {
    title: "CHEZ14's Code Style",
    slug: "phpcs",
    repoUrl: {
      repoType: "gitlab",
      url: "https://gitlab.com/chez14/phpcs",
      kind: "repo"
    },
    tags: ["code-style", "code-sniffer", "phpcs"],
    techStack: [
      knownLanguages['php'],
    ]
  }, {
    title: "Gratheus",
    slug: "gratheus",
    description: "Grafana + Prometheus dashboard for quick experiment monitoring",
    repoUrl: {
      repoType: "github",
      url: "https://github.com/chez14/gratheus",
      kind: "repo"
    },
    tags: ["monitoring", "grafana", "prometheus"],
    techStack: [
      { title: "Grafana", type: "other" },
      { title: "Prometheus", type: "other" },
      knownInfra['docker'],
    ]
  }, {
    title: "Collection of Indonesian Standards for Govt-related Things",
    slug: "indonesian-standards",
    description: "Helps developer gain quick technical reference for Gov-related documents.",
    repoUrl: {
      repoType: "github",
      url: "https://github.com/chez14/indonesian-standards",
      kind: "repo"
    },
    tags: ["standards", "goverment", "technical-document", "documentations"],
  }
];
