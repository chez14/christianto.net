
export interface BlogPost {
  id: number,
  link: string,
  date: string,
  title: string,
  excerpt: string,
  author: { name: string, link: string }[],
  featuredImage: { file: string, width: number, height: number, mime_type: string, source_url: string },
  categories: { name: string, link: string }[],
  tags: { name: string, link: string }[],
}

export interface BlogPostRaw {
  status: boolean,
  data: BlogPost[]
}
